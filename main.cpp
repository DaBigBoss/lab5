#pragma once
#include <iostream>

struct Object 
{
    double data;
    Object* next;
    Object() 
    {
        data=0;
        next=NULL;
    }
};

template <typename T>
class Stack 
{
private:
    Object* s_head;
public:
    Stack()
    {
        s_head=NULL;
    }
    void push(T&& value) 
    {
        Object* elem=new Object();
        elem->data=std::move(value);
        if(s_head==NULL)
        {
            s_head=elem;
        }
        else 
        {
            elem->next=s_head;
            s_head=elem;
        }
    }
    void push(const T& value) 
    {
        Object* elem=new Object();
        elem->data=value;
        if(s_head==NULL)
        {
            s_head=elem;
        }
        else 
        {
            elem->next=s_head;
            s_head=elem;
        }
    }
    void pop()
    {
        Object* buf=new Object();
        s_head=std::move(s_head->next);
    }
    const T& head() const 
    {
        return s_head->data;
    }
    template <typename ... Args>
    void push_emplace(Args&&... value) 
    {
        Object* elem=new Object();
        elem->data=T(std::forward<Args>(value)...);
        if(s_head==NULL)
        {
            s_head=elem;
        }
        else 
        {
            elem->next=s_head;
            s_head=elem;
        }
    }
    void print() 
    {
        std::cout << s_head->data;
        Object* buf=s_head;
        while(buf->next!=NULL)
        {
            std::cout << " - "  <<  buf->next->data;
            buf=buf->next;
        }
        std::cout << std::endl;
    }
};

int main()
{
    Stack<double> stack;
    stack.push(12);
    stack.push(6);
    stack.push(90);
    stack.push(22.3);
    stack.push(2);
    stack.print();
    std::cout << std::endl << stack.head() << std::endl;
    stack.pop();
    stack.pop();
    stack.print();
    stack.push_emplace<int> (10.5);
    stack.print();
    return 0;
}